## `Functional map`

![walk through](https://i.imgur.com/nUD5sp6.png "walk through")

---

## `Heroku deployment checklist`

* dynamic port binding

```js
const PORT = process.env.PORT || 5000;
app.listen(PORT);
```

* specify node environment
* specify start script

```js
package.json
{
  "name": "server",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "engines":{
    "node":"v10.0.0",
    "npm":"6.0.1"
  },
  "scripts": {
    "start": "node index.js"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "express": "^4.16.3",
    "nodemon": "^1.17.4",
    "passport": "^0.4.0",
    "passport-google-oauth20": "^1.0.0"
  }
}
```

* gitignore file
  > let heroku install all dependence

---

## `Heroku deployment`

* [heroku cli](https://devcenter.heroku.com/articles/heroku-cli)
* heroku login
* heroku create
  > https://sleepy-cliffs-26928.herokuapp.com/ | https://git.heroku.com/sleepy-cliffs-26928.git
* git remote add heroku \<git repo url>
* git push heroku master
* heroku open
* heroku logs

---

## `OAuth flow`

![oauth flow](https://lh3.googleusercontent.com/-juLrRv19aB4/VaNR-nSSMFI/AAAAAAAAPgM/_tBWS4QY89I/s387/google-api-flow-for-installed.png "oauth flow")

---

## `Passport`

* passport
  > General helpers for handing auth in express apps
* passport strategy
  > helpers for authenticating with one very specific method (email/password, google, facebook , etc)
* official site
  > http://www.passportjs.org/
* google oauth
  > npm i passport  
  > npm i passport-google-oauth20
* google apis console
  > https://console.developers.google.com/

---

## `Access Token 與 Refresh Token`

access token 通常生命期較短(google oauth 3600s)，萬一遭攔截或洩露時，可以減輕被駭客拿來濫用的情況。 而 refresh token （更新憑證）, 萬一遭攔截或洩露，不會有什麼做用，因為駭客必須同時取得 client id 和 secret 才能重新獲取 access token。 所以 refresh token 可以避免使用長生命週期 access token 可能造成的風險。

而且，當 access token 失效時，用戶端只要根據原有的 refresh token ，不需要再透過使用者，就可以重新取得新的 access token ，而不用打擾到使用者。
